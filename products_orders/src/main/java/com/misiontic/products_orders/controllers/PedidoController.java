package com.misiontic.products_orders.controllers;
import java.util.List;

import com.misiontic.products_orders.models.Pedido;
import com.misiontic.products_orders.repositories.PedidoRepository;
import org.springframework.web.bind.annotation.*;


@RestController
public class PedidoController {
    private final PedidoRepository pedidoRepository;

    public PedidoController(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

   
    @GetMapping("/pedidos/{username}")
    List<Pedido> getPedidosByUsername(@PathVariable String username){
        return pedidoRepository.getByUsername(username);
    }

    @PostMapping("/pedido/")
    Pedido newProduct(@RequestBody Pedido pedido){
        return pedidoRepository.save(pedido);
    }

    @PutMapping("/pedidos/{id}")
    Pedido updatePedido(@PathVariable String id, @RequestBody Pedido new_pedido){
        Pedido old_pedido = pedidoRepository.findById(id).orElse(null);
        old_pedido.setUsername(new_pedido.getUsername());
        old_pedido.setCantidad(new_pedido.getCantidad());
        old_pedido.setAbono(new_pedido.getAbono());
        old_pedido.setEstado(new_pedido.getEstado());
        old_pedido.setFecha_programada(new_pedido.getFecha_programada());
        old_pedido.setListProduct(new_pedido.getListProduct());
        old_pedido.setFecha_creacion(new_pedido.getFecha_creacion());
        return pedidoRepository.save(old_pedido);
    }

    @DeleteMapping("/pedidos/{id}")
    public void deletePedido(@PathVariable String id){

        pedidoRepository.deleteById(id);
        
    }


}
