package com.misiontic.products_orders.controllers;
import java.util.List;

import com.misiontic.products_orders.models.Product;
import com.misiontic.products_orders.repositories.ProductRepository;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/products/{username}")
    List<Product> getProductsByUsername(@PathVariable String username){
        return productRepository.getByUsername(username);
    }

    @GetMapping("/productsAll/")
    List<Product> getProductsByProduct(){
        return productRepository.findAll();//Repository tiene varios metodos
    }
    @PostMapping("/products")
    Product newProduct(@RequestBody Product product){
        return productRepository.save(product);
    }

    @PutMapping("/products/{id}")
    Product updateProduct(@PathVariable String id, @RequestBody Product new_product){
        Product old_product = productRepository.findById(id).orElse(null);
       // old_product.setId(new_product.getId());
        old_product.setName(new_product.getName());
        old_product.setImage(new_product.getImage());
        old_product.setValue(new_product.getValue());
        return productRepository.save(old_product);
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable String id){
        productRepository.deleteById(id);
    }


}
