package com.misiontic.products_orders.utils;
import java.text.Normalizer;

public class StringUtils {
    public StringUtils() {
    }

    public static String removeAccents(String inputStr){
        String inputStrNormalized = Normalizer.normalize(inputStr, Normalizer.Form.NFD);
        String inputStrWithoutAccents = inputStrNormalized.replaceAll("[^\\p{ASCII}]", "");
        return inputStrWithoutAccents;
    }
}