package com.misiontic.products_orders.repositories;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

import com.misiontic.products_orders.models.Product;


public interface ProductRepository extends MongoRepository <Product, String> {
    List<Product> getByUsername(String username);
    // ArrayList<Product> findByProduct();
}
