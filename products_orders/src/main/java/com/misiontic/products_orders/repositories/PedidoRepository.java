package com.misiontic.products_orders.repositories;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

import com.misiontic.products_orders.models.Pedido;


public interface PedidoRepository extends MongoRepository <Pedido, String> {
    List<Pedido> getByUsername (String username);//Se debe colocar el getBy y el nombre del parametro real
//    List<Product> getProducts();
}