package com.misiontic.products_orders.models;
import org.springframework.data.annotation.Id;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Product {
    @Id
    private String id;
    private String username;
    private String name;
    private String image;
    private Double value;

    public Product(String username, String name, String image, double value) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        DateTimeFormatter nowFormated = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
        // El id es ahora el nombre de usuario y la fecha actual de UTC, un ejemplo: 20211207-165705
        this.id = username + "-" + nowFormated.format(now);
        this.username = username;
        this.name = name;
        this.image = image;
        this.value = value;
    }

    public String getId() {
        return id;
    }



    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}