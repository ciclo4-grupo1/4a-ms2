package com.misiontic.products_orders.models;
import org.springframework.data.annotation.Id;
import java.util.ArrayList;
import java.util.Date;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Pedido {
    @Id
    private String id;
    private ArrayList<Product> listProduct= new ArrayList<Product>();
//    private String ListProduct;
    private String username;
    private int cantidad;
    private double abono;
    private String estado;
    private Date fecha_programada;
    private String fecha_creacion;

 
    public Pedido(ArrayList<Product> listProduct, String username, int cantidad, double abono, String estado, Date fecha_programada) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        DateTimeFormatter nowFormated = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
        String string =nowFormated.format(now);
        // LocalDate date = LocalDate.parse(string, nowFormated);
        this.fecha_creacion=string;
        this.id=username+ "_" +this.fecha_creacion;
        this.listProduct = listProduct;
        this.username = username;
        this.cantidad = cantidad;
        this.abono = abono;
        this.estado = estado;
        this.fecha_programada = fecha_programada;
       
        
    }

    public String getId() {
        return id;
    }

    public ArrayList<Product> getListProduct() {
        return listProduct;
    }

    public void setListProduct(ArrayList<Product> listProduct) {
        this.listProduct = listProduct;
    }
    // public String  getListProduct() {
    //     return ListProduct;
    // }

    // public void setListProduct(String listProduct) {
    //     ListProduct = listProduct;
    // }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getAbono() {
        return abono;
    }

    public void setAbono(double abono) {
        this.abono = abono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFecha_programada() {
        return fecha_programada;
    }

    public void setFecha_programada(Date fecha_programada) {
        this.fecha_programada = fecha_programada;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
}